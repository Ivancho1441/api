var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')

var server = require('npm start')

var should = chai.should()

chai.use(chaiHttp) // Configurar Chai con el modulo HTTP

describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://google.com.mx')
      .get('/')
      .end((err, res) => {
        console.log(res)
        res.should.have.status(200)
        done()
      })
  })
  it('Raiz de la API contesta', (done) => {
    chai.request('http:localhost:3000')
      .get('/v3')
      .end((err, res) => {
        console.log(res)
        res.should.have.status(200)
        res.body.should.be.a('array')
        done()
      })
  })
  it('Raiz de la API devuelve 2 colecciones', (done) => {
    chai.request('http:localhost:3000')
      .get('/v3')
      .end((err, res) => {
        console.log(res)
        res.should.have.status(200)
        res.body.should.be.a('array')
        res.body.length.should.be.eql(2)
        for (var i = 0 ; i < res.body.length ; i++){
          res.body[i].should.have.property('recurso')
          res.body[i].should.have.property(url)
        }
        done()
      })
  })
})

describe('Tests de Movimientos', () => {
  it ('Movimientos array', (done) => {
    chai.request('http:localhost:3000')
    .get('/v2/movimientos')
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('array')
    })
  })
  it ('Movimientos array por ID', (done) => {
    chai.request('http:localhost:3000')
    .get('/v2/movimientos/:0')
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('array')
    })
  })
})
